\LoadClass{article}
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{my_resume}[2017/7/17]
\RequirePackage{titlesec}
%\RequirePackage{fontawesome}
\RequirePackage[fixed]{fontawesome5}
\RequirePackage{setspace}
\RequirePackage{geometry}
\RequirePackage{hyperref}
\RequirePackage{xcolor}
\RequirePackage{comment}
\RequirePackage{enumitem}
\RequirePackage{graphicx}
\RequirePackage{etoolbox}
\RequirePackage{bold-extra}
\RequirePackage{multicol}
\RequirePackage{ifthen}
\usepackage[T1]{fontenc}

\geometry{
	left=0.5in,
	right=0.5in,
	top=0.5in,
	bottom=0.5in
}

\input{glyphtounicode}

\pdfgentounicode=1

\pagenumbering{gobble}
\setlength{\parskip}{0em}
%\setlength{\parsep}{0pt}
%\setlist{nosep}
\setlength{\multicolsep}{0pt}

%modifies .\section and .\subsection tags
\titleformat{\section}
	{\large\bfseries\raggedright}
	{}{0em}
	{}
	[\titlerule]
\titleformat{\subsection}
	{\scshape\normalsize\raggedright}
	{}{0em}
	{}
\titlespacing{\subsection}{0pt}{0.4em}{0pt}
\titlespacing{\section}{0pt}{0.5em}{1em}

% use this here to switch on graphics (if you're directly handing a recuiter a resume)
\newtoggle{showgraphics}
\toggletrue{showgraphics}
\togglefalse{showgraphics}

% either show or hide graphic depending on setting
\newcommand{\showGraphic}[1]{
	\iftoggle{showgraphics}{#1{}}{\hspace{-2px}}\hspace{-3px}
}

\newcommand{\tperiod}{{\large\textperiodcentered}}

%adds date to .\section and .\subsection respectively
\newcommand{\datedsection}[2]{%
	\section[#1]{#1 \hfill #2}%
}

\newcommand{\uofmdatedsection}[4]{%
	\setstretch{1.0}\subsection[#1]{#1 \hfill #4}\noindent \textit{#3} \hfill \textit{#2}%
}
\newcommand{\datedsubsection}[5]{%
	\setstretch{1.0}\subsection[#1]{#1 \hfill #4}\noindent \textit{#3} $|${\small\showGraphic{\faFileCode{}} #5} \hfill \textit{#2}
}

\newcommand{\positiononlysection}[3]{%
	\noindent \textit{#1} $|${\small\showGraphic{\faFileCode{}} #3} \hfill \textit{#2}
}

\newcommand{\project}[4]{%
	% \setstretch{1.0}\subsection[#1]{\href{#2}{#1} $|${\small\showGraphic{\faFileCode{}} \textnormal{#3}}}
	\setstretch{1.0}\subsection[#1]{\href{#2}{#1}}\noindent \textit{#4} $|${\small\showGraphic{\faFileCode{}} #3}
}

%generates name and contact info
\newcommand{\name}[1]{
	\centerline{\huge{#1}}
}
\newcommand{\contact}[6]{
	{\center{#2, #3 #4\\}}
	{\center{#5 {\large\textperiodcentered} #6 {\large\textperiodcentered} \faGithub{} \href{https://github.com/kir12}{\nolinkurl{kir12}} {\large\textperiodcentered} \faGitlab{} \href{https://gitlab.com/kir12}{\nolinkurl{kir12}} {\large\textperiodcentered} \faChrome{} \href{https://kir12.github.io}{\nolinkurl{kir12.github.io}} {\large\textperiodcentered} \faLinkedin{} \href{https://www.linkedin.com/in/kir12/}{\texttt{kir12}}\\}}
}

\newcommand{\skill}[3]{
	\begin{center}
		{\huge #1}\smallskip\linebreak
		{\small #2}\smallskip\linebreak
		\footnotesize #3
	\end{center}
}

\newcommand{\skillii}[2]{
	\begin{center}
		{\huge #1}\smallskip\linebreak
		\footnotesize #2
	\end{center}
}

%manipulates links
\hypersetup{
hidelinks=true,
colorlinks=true,
urlcolor=black
}

%minimizes bulletpoint lists
%\setitemize{noitemsep,topsep=0pt,parsep=0pt,partopsep=0pt,nolistsep}
\setitemize{topsep=0.5em,parsep=0pt,}

%COVER LETTER COMMANDS

\newcommand{\heading}{
	\name{Brian Lee}
	\contact{1670 Broadway Street Rm. 217-B}{Ann Arbor}{Michigan}{48105}{\faEnvelope{} \href{mailto:leebri@umich.edu}{\nolinkurl{leebri@umich.edu}}}{\faPhone{} \texttt{917-583-3990}}
}

\newcommand{\coverHead}{
	\setlength{\parskip}{0em}
	\heading{}
	\setlength{\parindent}{0em}
	\setlength{\parskip}{1em}
}
\newcommand{\address}[1]{

	\bigskip

	#1

	\medskip
	
}
\newcommand{\dear}[1]{
	#1
	
	\medskip	

}
\newcommand{\body}[1]{
	#1		
}
\newcommand{\coverEnd}[2]{
	
	#1

	\medskip

	{\Large #2}
}

\newtoggle{resume}
\togglefalse{resume}

\iftoggle{resume}{
	\titleformat{\section}
	{\normalfont\Large\bfseries}{\thesection}{1em}{}
	\titleformat{\subsection}
	{\normalfont\large\bfseries}{\thesubsection}{1em}{}
}


